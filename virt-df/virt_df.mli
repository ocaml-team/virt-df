(** 'df' command for virtual domains. *)
(* (C) Copyright 2007-2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

(** This module (Virt_df) contains functions and values which are
    used throughout the plug-ins and main code.
*)

val debug : bool ref			(** If true, emit debug info to stderr*)
val uri : string option ref		(** Hypervisor/libvirt URI. *)
val inodes : bool ref			(** Display inodes. *)
val human : bool ref			(** Display human-readable. *)
val all : bool ref			(** Show all or just active domains. *)
val test_files : string list ref	(** In test mode (-t) list of files. *)
val csv_mode : bool ref			(** CSV mode. *)
(** State of command line arguments. *)

val csv_write : (out_channel -> string list -> unit) option ref
(** If virt_df_csv.ml is compiled in then this hook is overridden with
    a function to write a single line to a CSV file.
*)
