# diskzip
# @configure_input@
# Copyright (C) 2007-2008 Red Hat Inc., Richard W.M. Jones
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

PACKAGE		:= @PACKAGE_NAME@
VERSION		:= @PACKAGE_VERSION@

INSTALL		:= @INSTALL@
HAVE_PERLDOC	:= @HAVE_PERLDOC@

prefix		= @prefix@
exec_prefix	= @exec_prefix@
bindir		= @bindir@

pkg_gettext     = @pkg_gettext@

#OCAMLCPACKAGES	:= -package unix,extlib,bitmatch -I ../lib
OCAMLCPACKAGES	:= -package unix,extlib -I +bitmatch -I ../lib

ifneq ($(pkg_gettext),no)
OCAMLCPACKAGES  += -package gettext-stub
endif

OBJS		:= diskzip_gettext.cmo diskzip.cmo
XOBJS		:= $(OBJS:.cmo=.cmx)

SYNTAX		:= -pp "camlp4o -I`ocamlc -where`/bitmatch pa_bitmatch.cmo"

OCAMLCFLAGS	:= @OCAMLCFLAGS@ $(SYNTAX)
#OCAMLCLIBS	:= -linkpkg diskimage.cma
OCAMLCLIBS	:= -linkpkg bitmatch.cma diskimage.cma

OCAMLOPTPACKAGES := $(OCAMLCPACKAGES)
OCAMLOPTFLAGS	:= @OCAMLOPTFLAGS@ $(SYNTAX)
#OCAMLOPTLIBS	:= -linkpkg diskimage.cmxa
OCAMLOPTLIBS	:= -linkpkg bitmatch.cmxa diskimage.cmxa

OCAMLDEPFLAGS   := $(SYNTAX)

OCAMLDOCFLAGS	:= $(SYNTAX) -I +extlib -I +bitmatch -I ../lib
OCAMLDOCFILES	:= diskzip.ml

BYTE_TARGETS	:= diskzip
OPT_TARGETS	:= diskzip.opt

ifeq ($(HAVE_PERLDOC),perldoc)
BYTE_TARGETS	+= diskzip.1 diskzip.txt
endif

all: $(BYTE_TARGETS)

opt: $(OPT_TARGETS)

diskzip: $(OBJS) ../lib/diskimage.cma
	ocamlfind ocamlc $(OCAMLCPACKAGES) $(OCAMLCFLAGS) $(OCAMLCLIBS) \
	  -o $@ $(OBJS)

diskzip.opt: $(XOBJS) ../lib/diskimage.cmxa
	ocamlfind ocamlopt \
	  $(OCAMLOPTPACKAGES) $(OCAMLOPTFLAGS) $(OCAMLOPTLIBS) \
	  -o $@ $(XOBJS)

# Manual page.
ifeq ($(HAVE_PERLDOC),perldoc)
diskzip.1: diskzip.pod
	pod2man -c "Disk images" --release "$(PACKAGE)-$(VERSION)" \
		$< > $@

diskzip.txt: diskzip.pod
	pod2text $< > $@
endif

install:
	if [ -x diskzip.opt ]; then \
	  mkdir -p $(DESTDIR)$(bindir); \
	  $(INSTALL) -m 0755 diskzip.opt $(DESTDIR)$(bindir)/diskzip; \
	fi

include ../Make.rules
