# virt-df
# Makefile.  Generated from Makefile.in by configure.
# Copyright (C) 2007-2008 Red Hat Inc., Richard W.M. Jones
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

PACKAGE		= virt-df
VERSION		= 2.1.1

INSTALL		= /usr/bin/install -c

SUBDIRS		= lib virt-df diskzip

all opt depend install doc:
	for d in $(SUBDIRS); do \
	  $(MAKE) -C $$d $@; \
	  if [ $$? -ne 0 ]; then exit 1; fi; \
	done

clean:
	for d in . $(SUBDIRS); do \
	  (cd $$d; rm -f *.cmi *.cmo *.cmx *.cma *.cmxa *.o *.a \
	    *.so *.opt *~ *.dll *.exe *.annot core); \
	done
	rm -f virt-df/virt-df
	rm -f diskzip/diskzip

distclean: clean
	rm -f config.h config.log config.status configure
	rm -rf autom4te.cache
	rm -f Makefile
	rm -f virt-df/Makefile

# Distribution.

dist:
	$(MAKE) check-manifest
	rm -rf $(PACKAGE)-$(VERSION)
	mkdir $(PACKAGE)-$(VERSION)
	tar -cf - -T MANIFEST | tar -C $(PACKAGE)-$(VERSION) -xf -
	$(INSTALL) -m 0755 configure $(PACKAGE)-$(VERSION)/
	tar zcf $(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)
	rm -rf $(PACKAGE)-$(VERSION)
	ls -l $(PACKAGE)-$(VERSION).tar.gz

check-manifest:
	hg manifest | sort > .check-manifest; \
	sort MANIFEST > .orig-manifest; \
	diff -u .orig-manifest .check-manifest; rv=$$?; \
	rm -f .orig-manifest .check-manifest; \
	exit $$rv

.PHONY: all opt depend install clean distclean configure dist check-manifest \
	release release_stage_2 release_stage_3 force