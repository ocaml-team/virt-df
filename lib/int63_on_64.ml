(* 63 bit signed integer type.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

(* This is the 64 bit implementation so for efficiency we used the
 * unboxed int type directly.
 *)
type t = int

module Operators = struct
  type int63 = t

  external ( +^ ) : int -> int -> int = "%addint"
  external ( -^ ) : int -> int -> int = "%subint"
  external ( *^ ) : int -> int -> int = "%mulint"
  external ( /^ ) : int -> int -> int = "%divint"
  external ( %^ ) : int -> int -> int = "%modint"
  external ( <^< ) : int -> int -> int = "%lslint"
  external ( >^> ) : int -> int -> int = "%lsrint"
  external ( &^ ) : int -> int -> int = "%andint"
  external ( |^ ) : int -> int -> int = "%orint"
  external ( ^^ ) : int -> int -> int = "%xorint"
  external ( ~^ ) : int -> int = "%identity"
  external ( ~^~ ) : int -> int = "%negint"
end

let zero = 0
let one = 1
let minus_one = ~1

external neg : int -> int = "%negint"

external add : int -> int -> int = "%addint"
external sub : int -> int -> int = "%subint"
external mul : int -> int -> int = "%mulint"
external div : int -> int -> int = "%divint"
external rem : int -> int -> int = "%modint"

external succ : int -> int = "%succint"
external pred : int -> int = "%predint"

let abs = abs

let max_int = max_int
let min_int = min_int

external logand : int -> int -> int = "%andint"
external logor : int -> int -> int = "%orint"
external logxor : int -> int -> int = "%xorint"
let lognot = lnot

external shift_left : int -> int -> int = "%lslint"
external shift_right : int -> int -> int = "%asrint"
external shift_right_logical : int -> int -> int = "%lsrint"

external of_int : int -> int = "%identity"
external to_int : int -> int = "%identity"
external of_float : float -> int = "%intoffloat"
external to_float : int -> float = "%floatofint"
external of_int32 : int32 -> int = "%int32_to_int"
external to_int32 : int -> int32 = "%int32_of_int"
external of_int64 : int64 -> int = "%int64_to_int"
external to_int64 : int -> int64 = "%int64_of_int"
external of_nativeint : nativeint -> int = "%nativeint_to_int"
external to_nativeint : int -> nativeint = "%nativeint_of_int"

external of_string : string -> int = "caml_int_of_string"
let to_string = string_of_int

(*external compare : int -> int -> int = "%compare"*)
(* I think it should be faster to use a specialized compare: *)
let compare : int -> int -> int = compare
