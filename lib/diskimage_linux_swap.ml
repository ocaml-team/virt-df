(* 'df' command for virtual domains.

   (C) Copyright 2007 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   Support for Linux swap partitions.
*)

open Diskimage_impl

open Int63.Operators

let id = "linux_swap"
let blocksize = ~^4096			(* XXX *)

let rec probe dev =
  (* Load the "superblock" (ie. first 0x1000 bytes). *)
  let bits = dev#read_bitstring ~^0 ~^0x1000 in

  bitmatch bits with
  | {
      (* Actually this isn't just padding. *)
      padding : 8*0x1000 - 10*8 : bitstring;
      ("SWAP-SPACE"|"SWAPSPACE2") : 80 : string
    } ->

      let fs_dev = new blocksize_overlay blocksize dev in
      {
	fs_cb = callbacks ();
	fs_dev = fs_dev;

	fs_blocksize = blocksize;
	fs_blocks_total = fs_dev#size /^ blocksize;

	(* The remaining fields are ignored when fs_is_swap is true. *)
	fs_is_swap = true;
	fs_blocks_reserved = ~^0;
	fs_blocks_avail = ~^0;
	fs_blocks_used = ~^0;
	fs_inodes_total = ~^0;
	fs_inodes_reserved = ~^0;
	fs_inodes_avail = ~^0;
	fs_inodes_used = ~^0;
      }

  | { _ } ->
      raise Not_found			(* Not Linux swapspace. *)

(* Linux swap space is always 'free', apart from the superblock.
 * Compare diskimage_linux_swsuspend.ml
 *)
and offset_is_free _ offset = offset >= blocksize

and callbacks =
  let i = ref 0 in
  fun () -> {
    fs_cb_uq = (incr i; !i);
    fs_cb_name = id;
    fs_cb_printable_name = "Linux swap";
    fs_cb_offset_is_free = offset_is_free;
  }

(* Register the plugin. *)
let () = register_plugin ~filesystem:probe id
