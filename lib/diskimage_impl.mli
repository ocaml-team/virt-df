(* (C) Copyright 2007-2008 Richard W.M. Jones, Red Hat Inc.
   http://libvirt.org/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

(* Don't use the functions and types in here directly.  The safe ones
 * are reexported through the Diskimage module, see diskimage.mli.
 *)

(**/**)

val debug : bool ref

(** {2 Device model} *)

class virtual device :
  object
    method virtual name : string
    method virtual size : Int63.t
    method read : Int63.t -> Int63.t -> string
    method read_bitstring : Int63.t -> Int63.t -> Bitmatch.bitstring
    method virtual blocksize : Int63.t
    method virtual map_block : Int63.t -> (device * Int63.t) list
    method virtual contiguous : Int63.t -> Int63.t
  end

class block_device : string -> Int63.t ->
  object
    method name : string
    method size : Int63.t
    method read : Int63.t -> Int63.t -> string
    method read_bitstring : Int63.t -> Int63.t -> Bitmatch.bitstring
    method blocksize : Int63.t
    method map_block : Int63.t -> (device * Int63.t) list
    method contiguous : Int63.t -> Int63.t
    method close : unit -> unit
  end

class offset_device : string -> Int63.t -> Int63.t -> Int63.t -> device ->
  object
    method name : string
    method size : Int63.t
    method read : Int63.t -> Int63.t -> string
    method read_bitstring : Int63.t -> Int63.t -> Bitmatch.bitstring
    method blocksize : Int63.t
    method map_block : Int63.t -> (device * Int63.t) list
    method contiguous : Int63.t -> Int63.t
  end

class blocksize_overlay : Int63.t -> device ->
  object
    method name : string
    method size : Int63.t
    method read : Int63.t -> Int63.t -> string
    method read_bitstring : Int63.t -> Int63.t -> Bitmatch.bitstring
    method blocksize : Int63.t
    method map_block : Int63.t -> (device * Int63.t) list
    method contiguous : Int63.t -> Int63.t
  end

val null_device : device

type machine = {
  m_name : string;
  m_disks : disk list;
  m_lv_filesystems :
    (lv * filesystem) list;
}

and disk = {
  d_name : string;
  d_dev : block_device;
  d_content : disk_content;
}

and disk_content =
    [ `Filesystem of filesystem
    | `Partitions of partitions
    | `PhysicalVolume of pv
    | `Unknown
    ]

and partitions = {
  parts_cb : partitioner_callbacks;
  parts_dev : device;
  parts : partition list;
}
and partition = {
  part_status : partition_status;
  part_type : int;
  part_dev : device;
  part_content : partition_content;
}

and partition_status = Bootable | Nonbootable | Malformed | NullEntry
and partition_content =
    [ `Filesystem of filesystem
    | `PhysicalVolume of pv
    | `Unknown
    ]

and filesystem = {
  fs_cb : filesystem_callbacks;
  fs_dev : device;
  fs_blocksize : Int63.t;
  fs_blocks_total : Int63.t;
  fs_is_swap : bool;
  fs_blocks_reserved : Int63.t;
  fs_blocks_avail : Int63.t;
  fs_blocks_used : Int63.t;
  fs_inodes_total : Int63.t;
  fs_inodes_reserved : Int63.t;
  fs_inodes_avail : Int63.t;
  fs_inodes_used : Int63.t;
}

and pv = {
  pv_cb : lvm_callbacks;
  pv_dev : device;
  pv_uuid : string;
}
and lv = {
  lv_dev : device;
}

(** {2 Table of callbacks from each type of plug-in} *)

and partitioner_probe = device -> partitions

and partitioner_callbacks = {
  parts_cb_uq : int;
  parts_cb_name : string;
  parts_cb_offset_is_free : partitions -> Int63.t -> bool;
}

and filesystem_probe = device -> filesystem

and filesystem_callbacks = {
  fs_cb_uq : int;
  fs_cb_name : string;
  fs_cb_printable_name : string;
  fs_cb_offset_is_free : filesystem -> Int63.t -> bool;
}

and lvm_probe = device -> pv

and lvm_callbacks = {
  lvm_cb_uq : int;
  lvm_cb_name : string;
  lvm_cb_list_lvs : pv list -> lv list;
  lvm_cb_offset_is_free : pv -> Int63.t -> bool;
}

val name_of_filesystem : filesystem -> string

(** {3 Plug-in registration} *)

val register_plugin :
  ?partitioner:partitioner_probe ->
  ?filesystem:filesystem_probe ->
  ?lvm:lvm_probe ->
  string -> unit

(** {3 Plug-in-specific data management. *)

val private_data_functions :
  ('key -> int) -> ('key -> 'data -> unit) * ('key -> 'data)

(** {2 Internal functions used by the plug-ins} *)

val canonical_uuid : string -> string
  (** Convert a UUID which may contain '-' characters to canonical form. *)

val group_by : ?cmp:('a -> 'a -> int) -> ('a * 'b) list -> ('a * 'b list) list
(** Group a sorted list of pairs by the first element of the pair. *)

val sort_uniq : ?cmp:('a -> 'a -> int) -> 'a list -> 'a list
(** [sort_uniq xs] returns the list [xs], sorted and with all duplicate
    elements removed. *)

val uniq : ?cmp:('a -> 'a -> int) -> 'a list -> 'a list
(** [uniq xs] removes adjacent duplicate elements from a list, like
    the Unix uniq(1) command. *)

val range : int -> int -> int list
(** [range a b] returns the list of integers [a <= i < b].
    If [a >= b] then the empty list is returned.
*)

(** {2 Functions} *)

val open_machine : string -> (string * string) list -> machine
val close_machine : machine -> unit
val scan_machine : machine -> machine

type ownership

val create_ownership : machine -> ownership

type owner =
    [ `Filesystem of filesystem
    | `Partitions of partitions
    | `PhysicalVolume of pv ]

val get_owners_lookup : machine -> ownership -> block_device ->
  (Int63.t -> (owner * Int63.t) list)
val offset_is_free : (owner * Int63.t) list -> bool
