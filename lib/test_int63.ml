open Printf
open Int63.Operators

let () =
  let a = ~^3 *^ ~^500 in
  let b = a <^< 3 in
  printf "result = %s\n" (Int63.to_string b)
