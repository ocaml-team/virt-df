(* 63 bit signed integer type.
   (C) Copyright 2008 Richard W.M. Jones, Red Hat Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

(* This is the 32 bit implementation so we have to use the boxed
 * and rather slow int64.
 *
 * Note that this code isn't quite correct, although not in a way
 * that anyone is likely to notice, in that we don't truncate the
 * underlying int64 to 63 bits on overflow.
 *)
type t = int64

module Operators = struct
  type int63 = t

  external ( +^ ) : int64 -> int64 -> int64 = "%int64_add"
  external ( -^ ) : int64 -> int64 -> int64 = "%int64_sub"
  external ( *^ ) : int64 -> int64 -> int64 = "%int64_mul"
  external ( /^ ) : int64 -> int64 -> int64 = "%int64_div"
  external ( %^ ) : int64 -> int64 -> int64 = "%int64_mod"
  external ( <^< ) : int64 -> int -> int64 = "%int64_lsl"
  external ( >^> ) : int64 -> int -> int64 = "%int64_lsr"
  external ( &^ ) : int64 -> int64 -> int64 = "%int64_and"
  external ( |^ ) : int64 -> int64 -> int64 = "%int64_or"
  external ( ^^ ) : int64 -> int64 -> int64 = "%int64_xor"
  external ( ~^ ) : int -> int64 = "%int64_of_int"
  let ( ~^~ ) i = Int64.neg (Int64.of_int i)
end

let zero = Int64.zero
let one = Int64.one
let minus_one = Int64.minus_one

external neg : int64 -> int64 = "%int64_neg"

external add : int64 -> int64 -> int64 = "%int64_add"
external sub : int64 -> int64 -> int64 = "%int64_sub"
external mul : int64 -> int64 -> int64 = "%int64_mul"
external div : int64 -> int64 -> int64 = "%int64_div"
external rem : int64 -> int64 -> int64 = "%int64_mod"

let succ = Int64.succ
let pred = Int64.pred

let abs = Int64.abs

(* XXX Should these return the 'real' 64 bit max/min int? *)
let max_int = Int64.pred (Int64.shift_left Int64.one 62)
let min_int = Int64.neg (Int64.shift_left Int64.one 62)

external logand : int64 -> int64 -> int64 = "%int64_and"
external logor : int64 -> int64 -> int64 = "%int64_or"
external logxor : int64 -> int64 -> int64 = "%int64_xor"
let lognot = Int64.lognot

external shift_left : int64 -> int -> int64 = "%int64_lsl"
external shift_right : int64 -> int -> int64 = "%int64_asr"
external shift_right_logical : int64 -> int -> int64 = "%int64_lsr"

external of_int : int -> int64 = "%int64_of_int"
external to_int : int64 -> int = "%int64_to_int"
external of_float : float -> int64 = "caml_int64_of_float"
external to_float : int64 -> float = "caml_int64_to_float"
external of_int32 : int32 -> int64 = "%int64_of_int32"
external to_int32 : int64 -> int32 = "%int64_to_int32"
external of_int64 : int64 -> int64 = "%identity"
external to_int64 : int64 -> int64 = "%identity"
external of_nativeint : nativeint -> int64 = "%int64_of_nativeint"
external to_nativeint : int64 -> nativeint = "%int64_to_nativeint"

external of_string : string -> int64 = "caml_int64_of_string"
let to_string = Int64.to_string

let compare : int64 -> int64 -> int = Int64.compare
